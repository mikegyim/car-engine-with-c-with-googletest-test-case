#include "CarEngine.hpp"

int main() {
    CarEngine myEngine(2.0, 4, 6000);
    myEngine.startEngine();
    myEngine.accelerate(2000);
    myEngine.displayEngineStatus();
    myEngine.decelerate(1000);
    myEngine.displayEngineStatus();
    return 0;
}

